-- set leader key to space
vim.g.mapleader = " "

local keymap = vim.keymap -- for conciseness

---------------------
-- General Keymaps -------------------

-- use jk to exit insert mode
keymap.set("i", "jk", "<ESC>", { desc = "Exit insert mode with jk" })

-- clear search highlights keymap.set("n", "<leader>nh", ":nohl<CR>", { desc = "Clear search highlights" })
-- delete single character without copying into register
keymap.set("n", "x", '"_x')

-- increment/decrement numbers
keymap.set("n", "<leader>+", "<C-a>", { desc = "Increment number" }) -- increment
keymap.set("n", "<leader>-", "<C-x>", { desc = "Decrement number" }) -- decrement

-- sauvegarde le fichier en cours a la mode windows
keymap.set("n", "<C-s>", "<cmd>w<CR>", { desc = "Sauvegarde le fichier en cours" }) -- decrement

-- scroll easier
-- keymap.set("n", "<silent> <S-C-j>", "<C-e>"
-- nnoremap <silent> <S-C-k> <C-y>
-- FIX ne fonctionne pas car le fonctionnement tmux-neovim prends le pas dessus
vim.keymap.set("n", "<A-j>", "<C-e>", { desc = "Scroll doux vers le bas" })
vim.keymap.set("n", "<A-k>", "<C-y>", { desc = "Scroll doux vers le haut" })

-- window management
keymap.set("n", "<leader>sv", "<C-w>v", { desc = "Split window vertically" }) -- split window vertically
keymap.set("n", "<leader>sh", "<C-w>s", { desc = "Split window horizontally" }) -- split window horizontally
keymap.set("n", "<leader>se", "<C-w>=", { desc = "Make splits equal size" }) -- make split windows equal width & height
keymap.set("n", "<leader>sx", "<cmd>close<CR>", { desc = "Close current split" }) -- close current split window

-- remplacement du mot survole par une saisie
keymap.set("n", "<leader>rr", function()
  local old_word = vim.fn.expand("<cword>")
  local new_word = vim.fn.input("Replace " .. old_word .. " by? ", old_word)
  if new_word ~= old_word and new_word ~= "" then
    vim.cmd(":%s/\\<" .. old_word .. "\\>/" .. new_word .. "/g")
  end
end, { desc = "Remplace le mot pointe par la saisie" })

-- remplacement du mot survole par une saisie avec confirmation
keymap.set("n", "<leader>rc", function()
  local old_word = vim.fn.expand("<cword>")
  local new_word = vim.fn.input("Replace " .. old_word .. " by? ", old_word)
  if new_word ~= old_word and new_word ~= "" then
    vim.cmd(":%s/\\<" .. old_word .. "\\>/" .. new_word .. "/gc")
  end
end, { desc = "Remplace le mot pointe et demande une confirmation" })

-- raccourcis pour la gestion des buffers
keymap.set("n", "<leader>bj", "<cmd>bnext<CR>", { desc = "Passe au buffer suivant" })
keymap.set("n", "<leader>bk", "<cmd>bprevious<CR>", { desc = "Passe au buffer precedent" })
keymap.set("n", "<leader>bd", "<cmd>bdelete<CR>", { desc = "Supprime le buffer actuel" })
keymap.set("n", "<leader>bl", "<cmd>ls<CR>", { desc = "Liste les buffers" })

-- raccourcis pour la gestion de copilot
keymap.set("n", "<leader>cc", "<cmd>lua require('copilot').copilot()<CR>", { desc = "Copilot" })
keymap.set("n", "<leader>cl", "<cmd>lua require('copilot').copilot(true)<CR>", { desc = "Copilot avec liste" })
-- en executant leacer cp on execute copilot panel

-- raccourcis pour enregistrer un fichier en root quand les droits ne sont pas permis
keymap.set("c", "w!!", "w !sudo tee % >/dev/null", { desc = "Enregistre le fichier en root" })
-- raccourcis pour recharger le fichier en cours d'edition
keymap.set("n", "<leader>eE", "<cmd>edit!<CR>", { desc = "Recharge le fichier en cours depuis le filesystem" })
keymap.set("n", "ee", "<cmd>edit!<CR>", { desc = "Recharge le fichier en cours depuis le filesystem" })
