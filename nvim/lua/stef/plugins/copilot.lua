return {
    "github/copilot.vim",
    config = function()
        require("gitsigns").setup({
            -- Navigation
            -- faire que controle+entree lance la commande :copilot panel

            vim.keymap.set("n", "]c", function()
                if vim.wo.diff then
                    return "]c"
                end
                return "<Ignore>"
            end, { expr = true }),
        })
    end,
}
