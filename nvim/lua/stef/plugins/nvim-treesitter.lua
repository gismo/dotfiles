return {
    {
        "nvim-treesitter/nvim-treesitter",
        event = { "BufReadPre", "BufNewFile" },
        build = ":TSUpdate",
        dependencies = {
            "nvim-treesitter/nvim-treesitter-textobjects",
            "windwp/nvim-ts-autotag",
        },
        config = function()
            -- import nvim-treesitter plugin
            local treesitter = require("nvim-treesitter.configs")

            -- configure treesitter
            treesitter.setup({ -- enable syntax highlighting
                highlight = { enable = true },
                update_focused_file = { enable = true },
                -- enable indentation
                indent = { enable = true },
                -- enable autotagging (w/ nvim-ts-autotag plugin)
                autotag = { enable = true },
                -- ensure these language parsers are installed
                ensure_installed = {
                    "json",
                    "javascript",
                    "typescript",
                    "tsx",
                    "yaml",
                    "html",
                    "css",
                    "prisma",
                    "markdown",
                    "markdown_inline",
                    "svelte",
                    "graphql",
                    "bash",
                    "lua",
                    "dockerfile",
                    "gitignore",
                    "query",
                    "groovy",
                },
                incremental_selection = {
                    enable = true,
                    keymaps = {
                        init_selection = "<C-space>",
                        node_incremental = "<C-space>",
                        scope_incremental = false,
                        node_decremental = "<bs>",
                    },
                },
                -- enable nvim-ts-context-commentstring plugin for commenting tsx and jsx
                -- context_commentstring = {
                --   enable = true,
                --   enable_autocmd = false,
                -- },
                require("ts_context_commentstring").setup({
                    enable_autocmd = false,
                    languages = {
                        typescript = "// %s",
                    },
                }),
            })

            local autocmd = vim.api.nvim_create_autocmd

            autocmd('BufRead', {
                pattern = 'Jenkinsfile*',
                command = 'set filetype=groovy'
            })

            autocmd('BufNewFile', {
                pattern = 'Jenkinsfile*',
                command = 'set filetype=groovy'
            })
        end,
    },
}
