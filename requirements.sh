#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail
#set -o xtrace

# TODO : verifier si le system est un apt like ou pacman ou ... pour utiliser le bon system d'installation

APT_PACKAGES=(
    shellcheck
    zsh
    tmux
    snapd
    git-extras
    bash-completion
    nodejs
    npm
    btop
    tmuxp
    cmake
    gettext
    fuse
    fd-find
    python3
    tldr
    pre-commit
    gitleaks
    tree
    ripgrep
    figlet
    ntp
    unzip
    neofetch
)

NPM_PACKAGES=(
    diff-so-fancy
    tree-sitter-cli
)

SNAP_PACKAGES=(
    k9s
    yq
)

for apt_package in "${APT_PACKAGES[@]}"; do
  echo "install ${apt_package}"
  sudo apt install -y "${apt_package}"
done

if [[ "$(command -v pre-commit)" ]]; then
    pre-commit install
fi

for snap_package in "${SNAP_PACKAGES[@]}"; do
  echo "install ${snap_package}"
  sudo snap install "${snap_package}"
done
for npm_package in "${NPM_PACKAGES[@]}"; do
  echo "install ${npm_package}"
  sudo npm install --global "${npm_package}"
done

# installation de neovim
curl -L https://github.com/neovim/neovim/releases/latest/download/nvim.appimage --output /usr/local/bin/nvim.appimage
chmod u+x /usr/local/bin/nvim.appimage
ln -s /usr/local/bin/nvim.appimage /usr/local/bin/nvim
# installation de sa dependance pour traitement des fichiers lua
curl -L https://github.com/JohnnyMorganz/StyLua/releases/download/v0.20.0/stylua-linux.zip --output /tmp/stylua-linux.zip && sudo unzip /tmp/stylua-linux.zip -d /usr/local/bin

git config --global core.pager "diff-so-fancy | less --tabs=4 -RFX"
git config --global interactive.diffFilter "diff-so-fancy --patch"

# installation de ohmyzsh
if [[ ! -e "$HOME/.oh-my-zsh/" ]]; then
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
fi

# autres instllations
# python -m pip install --upgrade pip
# python -m pip install

# installation de zsh-autocomplete
if [[ ! -e "$ZSH/plugins/zsh-autocomplete" ]]; then
    git clone --depth 1 -- https://github.com/marlonrichert/zsh-autocomplete.git "$ZSH/plugins/zsh-autocomplete"
fi

# installation du gestionnaire de plugins tmux
if [[ ! -e "$HOME/.tmux/plugins/tpm" ]]; then
    git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
fi
