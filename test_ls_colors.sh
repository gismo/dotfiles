#!/usr/bin/env bash

# Créer un répertoire temporaire pour le test
TEST_DIR=$(mktemp -d)

# Aller dans le répertoire temporaire
cd $TEST_DIR

# Extraire les extensions et types de fichiers de la variable LS_COLORS
IFS=':' read -r -a LS_COLORS_ARRAY <<< "$LS_COLORS"

# Fonction pour créer des fichiers avec une certaine extension
create_files() {
    local extension=$1
    # si extension contient un point, c'est une extension de fichier sinon c'est un type de fichier
    if [[ $extension == *.* ]]; then
        touch "a_file${extension}"
    else
        touch "${extension}"
    fi
}

# Fonction pour créer des fichiers spéciaux et répertoires
create_special_files() {
    mkdir dir1 dir2
    ln -s file1 symlink1
    ln -s /nonexistent symlink2
    mkfifo fifo1
    touch executable1
    chmod +x executable1
}

# Créer les fichiers de test pour les extensions définies dans LS_COLORS
for item in "${LS_COLORS_ARRAY[@]}"; do
    if [[ $item == *\** ]]; then
        extension=${item#*\*}
        extension=${extension%%=*}
        create_files "$extension"
    fi
done

# Créer les fichiers spéciaux et répertoires
create_special_files

# Afficher les couleurs pour différents types de fichiers
echo -e "\nTest des couleurs définies par LS_COLORS :"
echo "Répertoires :"
ls --color -d dir1 dir2
echo -e "\nLiens symboliques :"
ls --color symlink1 symlink2
echo -e "\nFifos :"
ls --color fifo1
echo -e "\nFichiers exécutables :"
ls --color executable1
echo -e "\nFichiers :"
ls --color *

# Nettoyer le répertoire temporaire après le test
cd ..
rm -rf $TEST_DIR

