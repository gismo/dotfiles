#!/usr/bin/env bash

DOTFILES="$(pwd)"
BACKUP_DIR=$HOME/dotfiles-backup

COLOR_GRAY="\033[1;38;5;243m"
COLOR_BLUE="\033[1;34m"
COLOR_GREEN="\033[1;32m"
COLOR_RED="\033[1;31m"
COLOR_PURPLE="\033[1;35m"
COLOR_YELLOW="\033[1;33m"
COLOR_NONE="\033[0m"

# usage
function usage {
    figlet -tk Installation Dotfiles
    cat <<EOF
Ce script permet d'installer un enviromment de travail d'un poste a l'autre en creant des liens symbolique vers les fichiers de configuration des logiciels habituels.

Usage : $0 {backup|link|dist|git|homebrew|shell|terminfo|remove|test|vim|neovim|install-test|cmnu|custom-my-new-ubuntu|all}

Option :
 backup                 install les backup              
 link                   install les link                
 dist                   install les dist                
 git                    install les git                 
 homebrew               install les homebrew            
 shell                  install les shell               
 terminfo               install les terminfo            
 remove                 install les remove              
 test                   install les test                
 vim                    install les vim                 
 neovim                 install les neovim              
 install-test           install les install-test        
 test                   install les test                
 cmnu                   install les cmnu                
 custom-my-new-ubuntu   install les custom-my-new-ubuntu
 all                    install toutes les configurations

Reminder: Valoriser les secrets dans .env avant de lancer le script

Exemples :

Exit status:
   0    OK,
   1    erreur
EOF
}

title() {
    echo -e "\n${COLOR_PURPLE}$1${COLOR_NONE}"
    echo -en "${COLOR_GRAY}"
    LONG=$(echo -n "${1}"|wc -m)
    for (( i = 1; i <= "${LONG}"; i++ )); do echo -n "="; done
    echo -e "${COLOR_NONE}\n"
}

error() {
    echo -e "${COLOR_RED}Error: ${COLOR_NONE}$1"
    exit 1
}

warning() {
    echo -e "${COLOR_YELLOW}Warning: ${COLOR_NONE}$1"
}

info() {
    echo -e "${COLOR_BLUE}Info: ${COLOR_NONE}$1"
}

success() {
    echo -e "${COLOR_GREEN}$1${COLOR_NONE}"
}

get_linkables() {
    find -H "$DOTFILES" -maxdepth 3 -name '*.symlink'
}

get_distables() {
    find -H "$DOTFILES" -maxdepth 3 -name '*.dist'
}


remove_links() {
    warning "/!\ Delete symlink"

    read -rn 1 -p "Are you sure to delete this files ? Are you BACKUP before ?? [y/N] " sure
    if [[ $sure =~ ^([Yy])$ ]]; then
        for file in $(get_linkables) ; do
            target="$HOME/.$(basename "$file" '.symlink')"
            if [ -e "$target" ]; then
                info "Deleting symlink for $file"
                rm "$target"
            else
                info "~${target#"$HOME"} don't exists... ${COLOR_PURPLE}Skipping.${COLOR_NONE}"
            fi
        done

        config_files=$(find "$DOTFILES/config" -maxdepth 1 2>/dev/null)
        for config in $config_files; do
            target="$HOME/.config/$(basename "$config")"
            if [ -e "$target" ]; then
                info "Deleting symlink for $config"
                rm "$target"
            else
                info "~${target#"$HOME"} don't exists... Skipping."
            fi
        done

        echo -e
        info "Creating vim symlinks"
        VIMFILES=( "$HOME/.vim:$DOTFILES/nvim"
                "$HOME/.vimrc:$DOTFILES/nvim/init.vim" )

        for file in "${VIMFILES[@]}"; do
            KEY=${file%%:*}
            VALUE=${file#*:}
            if [ -e "${KEY}" ]; then
                info "Deleting symlink for ${VALUE}"
                rm "${KEY}"
            else
                info "${KEY} don't exists... skipping."
            fi
        done
    else
        success "Mission aborted !"
    fi
}

backup() {
    echo "Creating backup directory at $BACKUP_DIR"
    mkdir -p "$BACKUP_DIR"

    for file in $(get_linkables); do
        filename=".$(basename "$file" '.symlink')"
        target="$HOME/$filename"
        if [ -f "$target" ]; then
            echo "backing up $filename"
            cp "$target" "$BACKUP_DIR"
        else
            warning "$filename does not exist at this location or is a symlink"
        fi
    done

    for filename in "$HOME/.config/nvim" "$HOME/.vim" "$HOME/.vimrc"; do
        if [ ! -L "$filename" ]; then
            echo "backing up $filename"
            cp -rf "$filename" "$BACKUP_DIR"
        else
            warning "$filename does not exist at this location or is a symlink"
        fi
    done
}


setup_dists() {
    title "Creating dists"

    for file in $(get_distables) ; do
        target="$HOME/$(basename --suffix=.dist "${file}")"
        if [ -e "$target" ]; then
            info "~${target#"$HOME"} already exists... ${COLOR_PURPLE}Skipping.${COLOR_NONE}"
        else
            info "${COLOR_GREEN}Creating dists ${COLOR_NONE} for $file"
            cp "${file}" "${target}"
        fi
    done
}

setup_symlinks() {
    title "Creating symlinks"

    for file in $(get_linkables) ; do
        target="$HOME/.$(basename "$file" '.symlink')"
        if [ -e "$target" ]; then
            info "~${target#"$HOME"} already exists... ${COLOR_PURPLE}Skipping.${COLOR_NONE}"
        else
            info "${COLOR_GREEN}Creating symlink${COLOR_NONE} for $file"
            ln -s "$file" "$target"
        fi
    done

    echo -e
    info "installing to ~/.config"
    if [ ! -d "$HOME/.config" ]; then
        info "Creating ~/.config"
        mkdir -p "$HOME/.config"
    fi

    config_files=$(find "$DOTFILES/config" -maxdepth 1 2>/dev/null)
    for config in $config_files; do
        target="$HOME/.config/$(basename "$config")"
        if [ -e "$target" ]; then
            info "~${target#"$HOME"} already exists... ${COLOR_PURPLE}Skipping.${COLOR_NONE}"
        else
            info "${COLOR_GREEN}Creating symlink${COLOR_NONE} for $config"
            ln -s "$config" "$target"
        fi
    done

    # create vim symlinks
    # As I have moved off of vim as my full time editor in favor of neovim,
    # I feel it doesn't make sense to leave my vimrc intact in the dotfiles repo
    # as it is not really being actively maintained. However, I would still
    # like to configure vim, so lets symlink ~/.vimrc and ~/.vim over to their
    # neovim equivalent.

    echo -e
    info "Creating vim symlinks"
    VIMFILES=( "$HOME/.config/vim:$DOTFILES/vim"
               "$HOME/.config/nvim:$DOTFILES/nvim"
               "$HOME/.vimrc:$DOTFILES/nvim/init.vim" )

    for file in "${VIMFILES[@]}"; do
        KEY=${file%%:*}
        TARGET=${file#*:}
        if [ -e "${KEY}" ]; then
            info "${KEY} already exists... ${COLOR_PURPLE}Skipping.${COLOR_NONE}"
        else
            info "Creating symlink for $KEY"
            ln -s "${TARGET}" "${KEY}"
        fi
    done
}

setup_git() {
    title "Setting up Git"

    rm ~/.gitconfig
    ln -s "${DOTFILES}/git/gitconfig.symlink" ~/.gitconfig

    defaultName=$(git config user.name)
    defaultEmail=$(git config user.email)
    defaultGithub=$(git config github.user)

    read -rp "Name [$defaultName] " name
    read -rp "Email [$defaultEmail] " email
    read -rp "Github username [$defaultGithub] " github

    git config -f ~/.gitconfig-local user.name "${name:-$defaultName}"
    git config -f ~/.gitconfig-local user.email "${email:-$defaultEmail}"
    git config -f ~/.gitconfig-local github.user "${github:-$defaultGithub}"
}

setup_homebrew() {
    title "Setting up Homebrew"

    if test ! "$(command -v brew)"; then
        info "Homebrew not installed. Installing."
        # Run as a login shell (non-interactive) so that the script doesn't pause for user input
        curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh | bash --login
    fi

    if [ "$(uname)" == "Linux" ]; then
        test -d ~/.linuxbrew && eval "$(~/.linuxbrew/bin/brew shellenv)"
        test -d /home/linuxbrew/.linuxbrew && eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
        test -r ~/.bash_profile && echo "eval \$($(brew --prefix)/bin/brew shellenv)" >>~/.bash_profile
    fi

    # install brew dependencies from Brewfile
    brew bundle

    # install fzf
    echo -e
    info "Installing fzf"
    "$(brew --prefix)"/opt/fzf/install --key-bindings --completion --no-update-rc --no-bash --no-fish
}

setup_shell() {
    title "Configuring shell"

    [[ -n "$(command -v brew)" ]] && zsh_path="$(brew --prefix)/bin/zsh" || zsh_path="$(which zsh)"
    if ! grep "$zsh_path" /etc/shells; then
        info "adding $zsh_path to /etc/shells"
        echo "$zsh_path" | sudo tee -a /etc/shells
    fi

    if [[ "$SHELL" != "$zsh_path" ]]; then
        chsh -s "$zsh_path"
        info "default shell changed to $zsh_path"
    fi
}

setup_terminfo() {
    title "Configuring terminfo"

    info "adding tmux.terminfo"
    tic -x "$DOTFILES/resources/tmux.terminfo"

    info "adding xterm-256color-italic.terminfo"
    tic -x "$DOTFILES/resources/xterm-256color-italic.terminfo"
}

check_alias() {
    alias "$1" || echo "Alias '$1' not found"
    return 0
}

check_all(){
    check_alias ta
    check_alias battery
}

setup_neovim(){
    title "setup neovim"
    ln -s "${DOTFILES}/nvim" ~/.config/nvim
    nvim --headless "+Lazy! sync" +qa
}

setup_vim(){
	mkdir -p ~/.vim/autoload ~/.vim/bundle ~/.vim/colors && \
	git clone --depth=1 https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim && \
	vim +PluginInstall +qall
}

install_dmenu(){
    title "install dmenu"
    install dmenu
}

install_bootloader_plymouth(){
    # make sure you have the packages for plymouth
    title "install plymouth"
    install plymouth

    # after downloading or cloning themes, copy the selected theme in plymouth theme dir
    local TMP_PLYMOUTH_THEME TMP_PLYMOUTH_THEME
    PATH_PLYMOUTH_THEME=/usr/share/plymouth/themes/
    PATH_PLYMOUTH_THEME=/usr/share/plymouth/themes/
    git clone --depth=1 https://github.com/adi1090x/plymouth-themes.git "$TMP_PLYMOUTH_THEME" && cd "$TMP_PLYMOUTH_THEME" || exit

    local THEMES=(
        pack_1/black_hud
        pack_1/cubes
        pack_2/glowing
        pack_2/green_blocks
    )

    for theme in "${THEMES[@]}"; do
        sudo cp -r "${theme}" "$PATH_PLYMOUTH_THEME"

        local NAME_THEME
        NAME_THEME="${theme//pack.*\//}"
        echo "update-alternatives --install ${PATH_PLYMOUTH_THEME}default.plymouth default.plymouth ${PATH_PLYMOUTH_THEME}$NAME_THEME/$NAME_THEME.plymouth 100"

        # install the new theme
        sudo update-alternatives --install ${PATH_PLYMOUTH_THEME}default.plymouth default.plymouth ${PATH_PLYMOUTH_THEME}"${NAME_THEME}"/"${NAME_THEME}".plymouth 100
    done

    cd - && rm -rf "${TMP_PLYMOUTH_THEME}"

    # select the theme to apply
    sudo update-alternatives --config default.plymouth
    #(select the number for installed theme)

    # update initramfs
    sudo update-initramfs -u
}

# TODO L’extension « Resource_Monitor@Ory0n » n’existe pas
install_extension_show_cpu_ram(){
    title "install extension show cpu ram"
    install chrome-gnome-shell
    git clone --depth=1 https://github.com/0ry0n/Resource_Monitor ~/.local/share/gnome-shell/extensions/Resource_Monitor@Ory0n
    gnome-extensions enable Resource_Monitor@Ory0n
}

install_extension_copy_history(){
    title "install copy history"
    install chrome-gnome-shell
    git clone --depth=1 https://github.com/Tudmotu/gnome-shell-extension-clipboard-indicator.git ~/.local/share/gnome-shell/extensions/clipboard-indicator@tudmotu.com
    gnome-extensions enable clipboard-indicator@tudmotu.com
}

install_codium(){
    title "install vscode"
    wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg | gpg --dearmor | sudo dd of=/etc/apt/trusted.gpg.d/vscodium.gpg
    echo 'deb https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/debs/ vscodium main' | sudo tee --append /etc/apt/sources.list.d/vscodium.list
    sudo apt update && sudo apt install codium
}

remove(){
    sudo apt remove -y "${1}"
}

install_snap(){
    sudo snap install "${1}"
}

install(){
    sudo apt install -y "${1}"
}

remove_gnome_dock(){
    title "remove gnome dock"

}

change_file_manager(){
    title "change file manager uninstall nautilus to install thunar..."
    install thunar
    remove nautilus

}

remove_thunderbird(){
    title "remove thunderbird"
    remove thunderbird
}

remove_firefox(){
    title "remove firefox"
    remove firefox
}

install_clients_whatsapp_signal(){
    title "install clients whatsapp signal"
    install_snap 'signal-desktop whatsdesk'
}

install_spotify(){
    title "install spotify"
    curl -sS https://download.spotify.com/debian/pubkey_0D811D58.gpg | sudo apt-key add -
    echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list
    sudo apt-get update && sudo apt-get install spotify-client
}

install_brasero(){
    title "install brasero"
    install brasero
}

install_nitrogen(){
    title "install nitrogen"
    install nitrogen
}

install_tmux(){
    title "install tmux"
    install tmux
}

install_git(){
    title "install git"
    install 'git git-extras'
}

install_brave(){
    title "install brave"
    sudo apt install apt-transport-https curl
    sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
    echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list
    sudo apt update && sudo apt install brave-browser
}

install_font_hack(){
    title "install font hack"
    install fonts-hack-ttf
}

install_alacritty(){
    title "install alacritty"
    sudo add-apt-repository ppa:mmstick76/alacritty
    sudo apt install alacritty
}

install_htop(){
    title "install htop"
    install htop
}

install_vim(){
    title "install vim"
    install vim
}

install_neovim(){
	PATH_USER=/usr/local/bin

    title "install neovim via appimage"
	install fuze
    curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim.appimage
    chmod u+x nvim.appimage
	sudo mv ./nvim.appimage "${PATH_USER}"
	sudo ln -s "${PATH_USER}/nvim.appimage" "${PATH_USER}/nvim"
	nvim --version
}

install_zsh(){
    title "install zsh"
    sudo apt install zsh
    sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
    git clone --depth=1 https://github.com/marlonrichert/zsh-autocomplete.git "${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autocomplete"
    chsh -s "$(which zsh)"
}

install_network_tools(){
    title 'intall arp-scan nmap'
    install 'arp-scan nmap'
}

install_setbg(){
    title "install script set background"
    sudo ln -s "${DOTFILES}/setbg.sh" /usr/local/bin/setbg

    echo "setbg ~/Images/wallpapers-dwt1 &" >> /tmp/setbg.sh
    sudo cp /tmp/setbg.sh /etc/profile.d/setbg.sh
}


case "$1" in
    backup)
        backup
        ;;
    link)
        setup_symlinks
        ;;
    dist)
        setup_dists
        ;;
    git)
        setup_git
        ;;
    homebrew)
        setup_homebrew
        ;;
    shell)
        setup_shell
        ;;
    terminfo)
        setup_terminfo
        ;;
    remove)
        remove_links
        ;;
    test)
        check_all
        ;;
    install-test)
        install_nitrogen
        install_setbg
        ;;
    cmnu)
        change_file_manager
        install_zsh
        install_vim
        install_htop
        install_alacritty
        install_font_hack
        install_brave
        install_brasero
        install_git
        install_tmux
        install_nitrogen
        install_setbg
        install_network_tools
        install_dmenu
        install_bootloader_plymouth
        install_spotify
        install_clients_whatsapp_signal
        remove_firefox
        remove_thunderbird
        remove_gnome_dock
        install_codium
        # install_extension_show_cpu_ram
        # install_extension_copy_history
        # install_raccourcits ubuntu
        ;;
    vim)
        install_vim
        setup_vim
        ;;
    neovim)
        #install_neovim
        setup_neovim
        ;;
    all)
        setup_symlinks
        setup_terminfo
        setup_shell
        setup_git
        ;;
    *)
        usage
        exit 0
        ;;
esac

echo -e
success "Done."
