# Feuille de route

Cette feuille de route des fonctionnalités vise à fournir une transparence sur ce qui est prévu d'être ajouté à Dotfiles dans un avenir proche. Notez qu'il n'y a pas de dates prévues pour les jalons des versions ci-dessous, l'expérience a montré que ces dates ne tiennent pas de toute façon.

## 0.9

Objectif principal : Sauvegarder la version fonctionnelle de ce projet de 3 ans.

-   Poser le tag 0.9.0
-   Mise en place d'un system industriel pour la gestion des versions

## 0.10.0

Objectif principal : Mettre a jour nvim et facilite sa page d'accueil

## 0.11.0

Objectif principal : gerer le timezones et les dates

## 1.0.0

Objectif principal : Avoir un systeme de deploiement de fichier robuste et fiable mis à jour sans la lourdeur de l'historique de ce projet

-   Passage a un systeme de deploiement de fichier plus robuste (stow)
-   Avoir une gestion systematique de backup restore pour tout fichier deploye (par projet : git, nvim, tmux, zsh, etc)
-   Avoir un environement de test pour verifier la robustesse du deploiement. Dockeriser pour proceder au deploiement
-   Que l'architecture porte les deploiement possible, exemple [de typecraft](https://github.com/typecraft-dev/dotfiles)
-   faire une installation a la `curl -L http://raw.github.com/username/repo/master/install.sh | sh` pour le deploiement
-   faire un systeme de mise a jour a la zsh qui permet de faire un git pull sur le repo et de mettre a jour les fichiers de configuration par la suite

## Backlog / Planned (unassigned)

Fonctionnalités qui POURRAIENT être ajoutées (même dans les versions mentionnées ci-dessus) ou rejetées

-   passage sur un systeme de synchronisation de fichier plus robuste utilise sur d'autres systemes tel que [stow](https://tamerlan.dev/how-i-manage-my-dotfiles-using-gnu-stow/)
-   partir sur un nouveau modele TDD pour la robustesse du deploiement de ce projet from scratch
-   ajouter dans la page alpha-nvim un system qui affichera la version de nvim en cours et si une version plus recente est disponible sur github
