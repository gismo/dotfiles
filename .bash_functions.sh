#!/usr/bin/env bash

meteo() {
  curl "wttr.in/${1:-Paris}?format=v2"
}

weather() {
  curl "wttr.in/${1:-Paris}"
}

#alias weather='curl wttr.in/Paris'
# Show mounts of one container, e.g., $dmounts $(docker ps -q |tail -n 1)
dmounts() {
  docker inspect --format='{{json .Mounts}}' $1 | jq
}

# Show stats with the names of the containers
dstats() {
  docker stats $(docker ps --format={{.Names}});
}

# Stop all containers
dstop() {
  docker stop $(docker ps -a -q);
}

# Remove all containers
drm() {
  docker rm $(docker ps -a -q);
}

# Remove all images
dri() {
  docker rmi $(docker images -q);
}

# Dockerfile build, e.g., $dbu tcnksm/test
dbu() {
  docker build -rm -t=$1 .;
}

# Show all alias related docker
dalias() {
  alias | grep 'docker' | sed "s/^\([^=]*\)=\(.*\)/\1 => \2/"| sed "s/['|\']//g" | sort;
}
