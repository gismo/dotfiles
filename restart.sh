#!/usr/bin/env bash

echo "Uptime : $(uptime --pretty), You ask to shutdown !"
echo "ok (y,n,r) ?"
echo "yes, no, reboot"
read ans

case "$ans" in
    n)
    echo "Mission abort ;)"
    ;;
    r)
    reboot
    ;;
    *)
    poweroff
    ;;
esac

#
#/usr/local/sbin
#/usr/local/bin
#/usr/sbin
#/usr/bin
#/sbin
#/bin
#/usr/games
#/usr/local/games
#/snap/bin
#/snap/bin
