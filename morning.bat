@echo off

::Decomposition de la date actuelle
set /a J=100%DATE:~0,2% %% 100
set /a M=100%DATE:~3,2% %% 100
set /a A=%DATE:~6,4%

::Calcul du jour julien
set /a JOUR_SEMAINE=(1461 * (%A% + 4800 + (%M% - 14) / 12)) / 4 + (367 * (%M% - 2 - 12 * ((%M% - 14) / 12))) / 12 - (3 * ((%A% + 4900 + (%M% - 14) / 12) / 100)) / 4 + %J% - 32075

::Calcul numero du jour [Lundi = 0 ... Dimanche = 6]
set /a JOUR_SEMAINE%%=7

:: Si c'est un lundi ou un mercredi ou un jeudi propose de ne pas lancer les logiciels non necessaire en presentiel
IF %JOUR_SEMAINE%==1 GOTO jourDeTt
IF %JOUR_SEMAINE%==4 GOTO jourDeTt
echo "Ce n'est pas un jour de teletravail"
GOTO nextStep

:jourDeTt
  :: demande a l'utilisateur s'il veut continuer
  set /p CONTINUER= "C'est un jour de teletravail, voulez-vous continuez ? [O/N]"
  :: verifie si l'utilisateur a repondu O en minuscul ou majuscule
  if /I %CONTINUER%==O GOTO lancementLogicielTt
  if /I %CONTINUER%==o GOTO lancementLogicielTt
  GOTO nextStep

:lancementLogicielTt
echo "Lancement des logiciels necessaire pour le teletravail"
echo.
Start "Pulse" "C:\Program Files (x86)\Common Files\Pulse Secure\JamUI\Pulse.exe" -show
Start "KeePass" "C:\Program Files\KeePass Password Safe 2\KeePass.exe"

:nextStep
echo "Lancement de toutes les applications"
echo.
Start "virtualBox" "C:\Program Files\Oracle\VirtualBox\VirtualBox.exe"
Start "chrome" "C:\Program Files\Google\Chrome\Application\chrome.exe"
Start "Outlook" "C:\Program Files\Microsoft Office\Office16\OUTLOOK.EXE"
start "Windows Terminal" "C:\portapps\terminal\WindowsTerminal.exe"

:: lancement du choix du navigateur par defaut
:: Start "" "%windir%\system32\control.exe" "/name" "Microsoft.DefaultPrograms" "/page" "pageDefaultProgram\pageAdvancedSettings?pszAppName=google%20chrome"

:: temps de veille en minutes
set TEMPS_OUT=420
:: eteindre l'ecran apres TEMPS_OUT minutes
powercfg -CHANGE -monitor-timeout-ac %TEMPS_OUT%
powercfg -CHANGE -monitor-timeout-dc %TEMPS_OUT%
:: mettre en veille apres TEMPS_OUT minutes
powercfg -CHANGE -standby-timeout-ac %TEMPS_OUT%
powercfg -CHANGE -standby-timeout-dc %TEMPS_OUT%
:: mettre en veille prolongee apres TEMPS_OUT minutes
powercfg -CHANGE -hibernate-timeout-ac %TEMPS_OUT%
powercfg -CHANGE -hibernate-timeout-dc %TEMPS_OUT%

echo "    _/_/_/"
echo "   _/    _/    _/_/    _/_/_/      _/_/"
echo "  _/    _/  _/    _/  _/    _/  _/_/_/_/"
echo " _/    _/  _/    _/  _/    _/  _/"
echo "_/_/_/      _/_/    _/    _/    _/_/_/  _/"

exit