#!/usr/bin/env bats

load 'test_helper/bats-support/load'
load 'test_helper/bats-assert/load'

# Load the script
# setup(){
#     source './install.sh'
# }

@test "l'exécution de install.sh simplement doit afficher la fonction usage d'aide" {
  run ./install.sh
  assert_output --partial "Ce script permet d'installer"
  assert_success
}

# @test "Test title function" {
#     result="$(title "Test Title")"
#     [ "$result" == $'\n\033[1;35mTest Title\033[0m\n\033[1;38;5;243m==========\033[0m\n' ]
# }
#
# @test "Test error function" {
#     run error "Test Error"
#     [ "$status" -eq 1 ]
#     [ "$output" == $'\033[1;31mError: \033[0mTest Error' ]
# }
#
# @test "Test warning function" {
#     result="$(warning "Test Warning")"
#     [ "$result" == $'\033[1;33mWarning: \033[0mTest Warning' ]
# }
#
# @test "Test info function" {
#     result="$(info "Test Info")"
#     [ "$result" == $'\033[1;34mInfo: \033[0mTest Info' ]
# }

# @test "Test success function" {
#     result="$(success "Test Success")"
#     [ "$result" == $'\033[1;32mSuccess: \033[0mTest Success' ]
# }
